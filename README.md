#AVRライタ開発用リポジトリ  
**回路はKiCADで設計**

##circuit  
	回路データディレクトリ  
	とにかく小型化し、50mm四方。LEDもつけて簡単な動作確認もできる。  
	書き込み方法は、  
	1.FTDIケーブルを回路に接続する。すると電源が入る。  
	2.AVRISPmkIIを接続する。  
	3.ルート権限で、avrerを実行。  

##src
	書き込み用ファーム、avrer.sh用ディレクトリ  
	ぶっちゃけディレクトリにするほどたくさんファイルがあるわけでもないが、なんとなく作った。  
	シェルスクリプトで記述されており、オプションをつけることでコンパイル、リンク、オブジェクトファイル生成ができる。
