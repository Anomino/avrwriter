#!/bin/bash
#AVR書き込み用のファーム

Warning()
{
	case $1 in
		usage)
			# 使い方
			echo "Usage : [command] [option] [files...]"
			;;
		*) 
			;;
	esac
}

Compile()
{
	# コンパイルし、オブジェクトファイルを生成
	avr-gcc -g -O$4 -mmcu=$2 -D F_CPU=$3 -c $1.cpp
}

Link()
{
	# 与えられたオブジェクトファイルをリンクし、実行ファイル生成


WriteAVR()
{
}

WriteFuse()
{

}
if test $# -lt 2
then
	Warning usage
fi

case $1 in
	-c)
		# コンパイル
		Compile $2 $3 $4 $5
		;;
	-l)
		# リンク
		Link
		;;
	-t)
		# AVR書き込み
		WriteAVR
		;;
	-f)
		# ヒューズビットの書き換え
		;;
	*)
		# 引数エラー
		Warning usage
		;;
esac
